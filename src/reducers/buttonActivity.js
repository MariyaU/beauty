import { CHANGE_ACTIVE_BUTTON } from '../constants/constants.js';

var initialState = [];
var miniarray = [1, 1, 1, 1, 1];
for (let i = 0; i < 4; i++) {
    initialState.push(miniarray.slice());
}
const buttonActivity = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_ACTIVE_BUTTON:
            let stateCopy = state.slice();
            stateCopy[action.keyType][action.keyTime] = !stateCopy[action.keyType][action.keyTime];
            return stateCopy;
        default:
            return state;
    }
};

export default buttonActivity;