import { combineReducers } from 'redux';

// import itemData from './itemData.js';
import tableData from './tableData.js';
import buttonActivity from './buttonActivity.js';
import { firestoreReducer } from 'redux-firestore';

const reducer = combineReducers({
    // itemData,
    tableData,
    buttonActivity,
    firestore: firestoreReducer
});

export default reducer;