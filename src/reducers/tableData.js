import {
    ADD_TABLE_ITEM,
    DELETE_TABLE_ITEM
} from '../constants/constants.js';

var initialTableData = [];

const tableData = (state = initialTableData, action) => {
    switch (action.type) {
        case ADD_TABLE_ITEM:
            let stateCopy = [];
            stateCopy = state.slice();
            stateCopy.push(action.item);
            return stateCopy;
        case DELETE_TABLE_ITEM:
            let stateCopyr = [];
            for (let i = 0; i < state.length; i++) {
                stateCopyr.push(state[i].slice());
            }
            stateCopyr.splice(action.id, 1);
            return stateCopyr;
        default:
            return state;
    }
};

export default tableData;