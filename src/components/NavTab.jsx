import React from 'react';
import { NavLink } from 'react-router-dom';

const NavTab = props => {
    return (
    <NavLink className = 'link' to = {props.to}>{props.label}</NavLink>
    )
};

export default NavTab;