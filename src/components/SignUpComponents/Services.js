import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import ServiceList from './ServiceList';

const Services = ({ services }) => {    
        // const { services } = props;
        // console.log('ser', services);
        return (
            <ServiceList list = {services}/>
        )
}

const mapStateToProps = (state) => {
    const services = state.firestore.ordered.services;
    return {
        services: services
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        { collection: 'services' }
    ])
)(Services);