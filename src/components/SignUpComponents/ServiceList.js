import React from 'react';
import ButtonsTime from './ButtonsTime';

const ServiceList = ({ list }) => {
    // console.log('list', list);
    return (
        <>
            {list && list.map((item, index) => {
                return (
                    <div key={item.id} className='container'>
                        <div key={item.id}>{item.title}</div>
                        <ButtonsTime type = {item.title} activity = {item.time} times={Object.keys(item.time)}/>                        
                    </div>
                )
            })}
        </>
    );
};

export default ServiceList;