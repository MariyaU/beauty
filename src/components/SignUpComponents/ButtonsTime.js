import React, { Component } from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';

class ButtonsTime extends Component {
    constructor(props) {
        super(props)
        this.state = { activity: [] }
    }

    render() {
        return (
            <>
                {this.props.times.map((item, index) => {
                    return (
                        <button disabled={this.state.activity[index]} key={index} className='waves-effect waves-light btn-small'>{item}</button>
                    )
                })}
            </>
        )
    }

}

// const ButtonsTime = (props) => {
//     console.log(props);
//     let activity = [];
//    for (let key in props.activity) {
//         activity.push(props.activity[key])
//     }
//     console.log(activity);
//     return (
//         <>
//             {props.times.map(function (item, index) {
//                 return (
//                     <button disabled={props.activity[item]} key={index} className='waves-effect waves-light btn-small'>{item}</button>
//                 )
//             })}
//         </>
//     )
// }

const mapStateToProps = (state) => {
    // console.log('from button',state)
    return {

    }
}
export default connect(mapStateToProps)(ButtonsTime);