import React from 'react';

const ButtonSend = props => {
    return (
        <button onClick = {() => props.onSend(props.data)} id='send'>{props.name}</button>
    );
};

export default ButtonSend;