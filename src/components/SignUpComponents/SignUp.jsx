import React from 'react';
import PersonalDataList from './PersonalDataList.jsx';
import Services from './Services';

const SignUp = () => {
    return (
        <div>
            <Services />
            <PersonalDataList />
        </div>
    )
};

export default SignUp;