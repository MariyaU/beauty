import React, { Component } from 'react';
import { enroll } from '../../actions/enrollActions';
import { connect } from 'react-redux';

class PersonalDataList extends Component {
    state = {
        firstName: '',
        secondName: '',
        phone: '',
    }
    handleChange = (e) => {
        console.log(e.target.value);
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
        this.props.createClient(this.state);        
    }
    render() {
        return (
            <div className='container'>
                <form className='white' onSubmit={this.handleSubmit}>
                    <h5 className='grey-text text-darken-3'>Регистрация</h5>
                    <div className='input-field'>
                        <label htmlFor='firstname'>First name</label>
                        <input type='text' id='firstName' onChange={this.handleChange} />
                    </div>
                    <div className='input-field'>
                        <label htmlFor='secondName'>Second name</label>
                        <input type='text' id='secondName' onChange={this.handleChange} />
                    </div>
                    <div className='input-field'>
                        <label htmlFor='phone'>Phone</label>
                        <input type='tel' id='phone' onChange={this.handleChange} />
                    </div>
                    <div className='input-field'>
                        <button className='btn pink lighten-1 z-depth-0'>Submit</button>
                    </div>
                </form>
            </div>
        )
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        createClient: (client) => dispatch(enroll(client))
    }
}

export default connect(null, mapDispatchToProps)(PersonalDataList);