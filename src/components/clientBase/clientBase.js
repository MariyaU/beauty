import React from 'react';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import UsersList from './usersList';

const ClientBase = (props) => {
    const { users } = props;
    console.log(users);
    return (
        <div className='container'>
            <table className='highlight'>
                <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Услуга</th>
                        <th>Время</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                    </tr>
                </thead>
                <tbody>
                    <UsersList users = {users}/>
                </tbody>
            </table>
        </div>
    )
}

const mapStateToProps = (state, ownProps) => {
    // console.log(state);
    const users = state.firestore.ordered.users;
    return {
        users: users
    }
}
export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        { collection: 'users' }
    ])
)(ClientBase);