import React from 'react';
import OneUser from './oneUser';

const UsersList = ({ users }) => {
    return (
        <>
            {users && users.map(user => {
                return (
                    <tr>
                        <OneUser user={user} />
                    </tr>
                )
            })
            }
        </>
    )
}

export default UsersList;