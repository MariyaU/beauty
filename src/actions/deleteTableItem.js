import { DELETE_TABLE_ITEM } from '../constants/constants.js';

const deleteTableItem = (id) => {
    return {
        type: DELETE_TABLE_ITEM,
        id: id
    };
};

export default deleteTableItem;