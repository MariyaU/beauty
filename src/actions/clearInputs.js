import { CLEAR_INPUTS } from '../constants/constants.js';

const clearInputs = () => {
    return {
        type: CLEAR_INPUTS
    };
};

export default clearInputs;