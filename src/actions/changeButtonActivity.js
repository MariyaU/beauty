import { CHANGE_ACTIVE_BUTTON } from '../constants/constants.js';

const changeButtonActivity = (keyType, keyTime) => {
    return {
        type: CHANGE_ACTIVE_BUTTON,
        keyType: keyType,
        keyTime: keyTime
    };
};

export default changeButtonActivity;